using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision) //Upon colliding with the invisible object (trigger) between the pipes
    {
        Score.score++; //score increases
    }
}
