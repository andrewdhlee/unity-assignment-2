using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Various variables

    public float MovementSpeed = 1;
    public float JumpForce = 1;

    private Rigidbody2D rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }


    private void Update()
    {
        //Moves the character along Horizontal axis aka Left & Right
        var movement = Input.GetAxis("Mouse X") * 5;
        transform.Translate (new Vector3(movement, 0, 0) * Time.deltaTime * MovementSpeed);

        //Moves the player upward & acts as jumping
        if (Input.GetKeyDown(KeyCode.Mouse0)) //&& Mathf.Abs(_rigidbody.velocity.y) < 0.001f)
        {
            rigidbody.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
        }
    }
}
