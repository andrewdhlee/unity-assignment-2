using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject newEnemy;
    public GameObject player;

    //Collision between Enemy and the Player creates a new Enemy to spawn
    //But the new Enemy will disappear after 5 seconds
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")   
        {
            Destroy(Instantiate(newEnemy), 5);      

        }
    }

    //Trigger between the Enemy and entering the Spawn Zone (surrounds the player)
    //If the Enemy enters the Spawn Zone, then the Player turns red
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Spawn Zone")
        {
            GameObject.Find("Player").GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    //If the Enemy is no longer in the Spawn Zone, then the player turns back white
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Spawn Zone")
        {
            GameObject.Find("Player").GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
}
