using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1 : MonoBehaviour
{

    public float HoritonztalThrust;
    public float maxVerticalThrust;

    float verticalThrust;
    float horizontalAxis;

    Rigidbody2D rigidbody;


    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        horizontalAxis = Input.GetAxis("Horizontal");
        if (Input.GetButton("Jump"))
        {
            verticalThrust = maxVerticalThrust;
        }
        else
        {
            verticalThrust = 0;
        }

    }

    private void FixedUpdate()
    {
        rigidbody.AddForce(new Vector2(horizontalAxis * HoritonztalThrust, verticalThrust));
    }
}
