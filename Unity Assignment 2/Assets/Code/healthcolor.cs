using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthcolor : MonoBehaviour
{
    public float color = 0;
    bool up = true;

    // Update is called once per frame
    void Update()
    {
        if (color >= 200)
        {
            up = false;
        }
        else if (color <= 100)
        {
            up = true;
        }

        if (up)
        {
            color = Mathf.Lerp(color, 255, 0.5f);
        }
        else if (!up)
        {
            color = Mathf.Lerp(color, 0, 0.5f);
        }

        GetComponent<Image>().color = new Color(color, 255-color, color);
    }
}
