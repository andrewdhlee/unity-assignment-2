using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    GameObject player;

    void Start()
    {
        player = GameObject.Find("Player"); //Value shown is for player

    }

    void Update()
    {
        GetComponent<Text>().text = "Height: " + player.transform.position.y; //Presents the Y position(height)
                                                                              //of the player
    }
}
