using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToFly : MonoBehaviour
{
    //Different variables established to be set into the character
    public YouDeaded youDead;
    public float velocity = 1;
    private Rigidbody2D rb;
    public float rotation = 0;
    public float goToRotation = 0;
    public float elapsedFrames = 0;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        youDead = GameObject.FindObjectOfType<YouDeaded>(); //Game Over screen was in different screen so this helped to try & resolve it
    }


    void Update()
    {
        elapsedFrames++; //Attempt to slow down the spinning feature
        if (elapsedFrames == 100)
        {
            elapsedFrames = 0;
        }

        if (Input.GetMouseButtonDown(0)) //Upon clicking left mouse button
        {
            rb.velocity = Vector2.up * velocity; //Apply velocity upward
        }

        if (Input.GetMouseButtonDown(1)) //Upon pressing the right mouse button
        {
            goToRotation += 45; //The squid will rotate by 45 degrees
        }

        transform.Rotate(0, 0, rotation); //The squid will rotate on z axis to make it look "clockwise"
        rotation = Mathf.LerpAngle(rotation, goToRotation, elapsedFrames/100); //The squid will be spinning repeatedly
    }

    private void OnCollisionEnter2D(Collision2D collision) //Upon hitting the floor or pipe  
    {
        youDead.GameOver(); //"Game Over" occurs
    }
}