using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Infinite : MonoBehaviour
{
    //Different variables set regarding pipe's properties
    public float maxTime = 1;
    private float timer = 0;
    public GameObject pipe;
    public float height;

    void Start()
    {
        
    }


    void Update()
    {
        //Creates a new pipe repeatedly at set times & varying their heights
        if (timer > maxTime)
        {
            GameObject newpip = Instantiate(pipe);
            newpip.transform.position = transform.position + new Vector3(0, Random.Range(-height * 3, height * 3), 0); //Create the pipe under random visual properties
            Destroy(pipe, 15); //Pipe will "disappear" after alotted time
            timer = 0;
        }

        timer += Time.deltaTime;
    }
}
